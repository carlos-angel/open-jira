interface SeedEntry {
  description: string;
  status: string;
  createdAt: number;
}

interface SeedData {
  entries: SeedEntry[];
}

export const seedData: SeedData = {
  entries: [
    {
      description: 'Ex culpa nulla laboris exercitation ad.',
      status: 'pending',
      createdAt: Date.now(),
    },
    {
      description: 'Commodo nulla velit voluptate adipisicing dolor esse dolor.',
      status: 'in-progress',
      createdAt: Date.now() - 1000000,
    },
    {
      description: 'Nisi ex qui consequat cupidatat ex veniam laborum ipsum pariatur et.',
      status: 'finished',
      createdAt: Date.now() - 100000000,
    },
  ],
};
