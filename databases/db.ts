import mongoose from 'mongoose';

/**
 * * 0: disconnected
 * * 1: connected
 * * 2: connecting
 * * 3: disconnecting
 */

const mongoConnection = {
  isConnected: 0,
};

export const connectDatabase = async () => {
  if (mongoConnection.isConnected) {
    console.info('ya estamos conectados');
    return;
  }

  if (mongoose.connections.length > 0) {
    mongoConnection.isConnected = mongoose.connections[0].readyState;

    if (mongoConnection.isConnected === 1) {
      console.info('usando conexión anterior');
      return;
    }

    await mongoose.disconnect();
  }

  // TODO: definir la uri en una variable de entorno
  await mongoose.connect(process.env.MONGO_URI!);
  mongoConnection.isConnected = 1;
  console.info('conectando la base de datos');
};

export const disconnectDatabase = async () => {
  if (process.env.NODE_ENV === 'development') return;

  if (mongoConnection.isConnected === 0) return;

  await mongoose.disconnect();
  mongoConnection.isConnected = 0;
  console.info('desconectando la base de datos');
};
