import { isValidObjectId } from 'mongoose';
import { Entry, IEntry } from '../models';
import { connectDatabase, disconnectDatabase } from './db';
export const getEntryById = async (id: string): Promise<IEntry | null> => {
  if (!isValidObjectId(id)) {
    return null;
  }

  await connectDatabase();

  const entry = await Entry.findById(id).lean();

  await disconnectDatabase();

  return JSON.parse(JSON.stringify(entry));
};
