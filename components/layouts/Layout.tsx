import { ReactNode } from 'react';
import { Box } from '@mui/material';
import Head from 'next/head';
import { Navbar, Sidebar } from '../ui';

interface Props {
  title: string;
  children: ReactNode;
}

export default function Layout({ title, children }: Props) {
  return (
    <Box sx={{ flexFlow: 1 }}>
      <Head>
        <title>OpenJira | {title}</title>
      </Head>
      <Navbar />
      <Sidebar />
      <Box sx={{ padding: '10px 20px' }}>{children}</Box>
    </Box>
  );
}
