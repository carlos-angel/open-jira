import {
  Drawer,
  Box,
  Typography,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Divider,
} from '@mui/material';
import InboxOutlinedIcon from '@mui/icons-material/InboxOutlined';
import { useUI } from '../../hooks';

const menuItems: string[] = ['Inbox', 'Starred', 'Send Email', 'Drafts'];

export default function Sidebar() {
  const { sideMenuOpen, closeSidebar } = useUI();
  return (
    <Drawer anchor='left' open={sideMenuOpen} onClose={closeSidebar}>
      <Box sx={{ width: 250 }}>
        <Box sx={{ padding: '5px 10px' }}>
          <Typography variant='h4'>Menú</Typography>
        </Box>

        <List>
          {menuItems.map((menu, i) => (
            <ListItem button key={`${menu}-${i}`}>
              <ListItemIcon>
                <InboxOutlinedIcon />
              </ListItemIcon>
              <ListItemText primary={menu} />
            </ListItem>
          ))}
        </List>

        <Divider />

        <List>
          {menuItems.map((menu, i) => (
            <ListItem button key={`${menu}-${i}`}>
              <ListItemIcon>
                <InboxOutlinedIcon />
              </ListItemIcon>
              <ListItemText primary={menu} />
            </ListItem>
          ))}
        </List>
      </Box>
    </Drawer>
  );
}
