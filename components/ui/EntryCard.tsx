import { DragEvent } from 'react';
import { Card, CardActionArea, CardActions, CardContent, Typography } from '@mui/material';
import { Entry } from '../../interfaces';
import { useUI } from '../../hooks';
import { useRouter } from 'next/router';
import { getFormatDistanceToNow } from '../../utils';

interface Props {
  entry: Entry;
}

export default function EntryCard({ entry }: Props) {
  const { onToggleDragging } = useUI();
  const router = useRouter();

  const onDragStart = (e: DragEvent) => {
    e.dataTransfer.setData('idEntry', entry._id);
    onToggleDragging();
  };

  const onDragEnd = () => {
    onToggleDragging();
  };

  const onClick = () => {
    router.push(`/entries/${entry._id}`);
  };

  return (
    <Card
      sx={{ marginBottom: 2 }}
      draggable
      onDragStart={onDragStart}
      onDragEnd={onDragEnd}
      onClick={onClick}
    >
      <CardActionArea>
        <CardContent>
          <Typography sx={{ whiteSpace: 'pre-line' }}>{entry.description}</Typography>
        </CardContent>

        <CardActions sx={{ display: 'flex', justifyContent: 'flex-end', paddingRight: 2 }}>
          <Typography variant='body2'>{getFormatDistanceToNow(entry.createdAt)}</Typography>
        </CardActions>
      </CardActionArea>
    </Card>
  );
}
