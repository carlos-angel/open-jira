import { useContext } from 'react';
import { EntriesContext } from '../context/entries';

export default function useEntries() {
  return useContext(EntriesContext);
}
