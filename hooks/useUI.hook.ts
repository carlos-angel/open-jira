import { useContext } from 'react';
import { UIContext } from '../context/ui';

export default function useUI() {
  return useContext(UIContext);
}
