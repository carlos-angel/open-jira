import { Entry } from '../../interfaces';
import { EntriesState } from './entries.provider';

type EntriesAction =
  | { type: 'addEntry'; payload: Entry }
  | { type: 'updateEntry'; payload: Entry }
  | { type: 'refresh-data'; payload: Entry[] };

export function entriesReducer(state: EntriesState, action: EntriesAction): EntriesState {
  switch (action.type) {
    case 'addEntry':
      return { ...state, entries: [...state.entries, action.payload] };
    case 'updateEntry':
      return {
        ...state,
        entries: state.entries.map((entry) =>
          entry._id === action.payload._id ? action.payload : entry,
        ),
      };
    case 'refresh-data':
      return {
        ...state,
        entries: [...action.payload],
      };
    default:
      return state;
  }
}

function addEntryAction(payload: Entry): EntriesAction {
  return {
    type: 'addEntry',
    payload,
  };
}

function updateEntryAction(payload: Entry): EntriesAction {
  return {
    type: 'updateEntry',
    payload,
  };
}

function refreshDataAction(payload: Entry[]): EntriesAction {
  return {
    type: 'refresh-data',
    payload,
  };
}

export const entriesTypes = {
  addEntryAction,
  updateEntryAction,
  refreshDataAction,
};
