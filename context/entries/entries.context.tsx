import { createContext } from 'react';
import { Entry } from '../../interfaces';

interface EntriesContextProps {
  entries: Entry[];
  addEntry: (description: string) => void;
  updateEntry: (entry: Entry, showSnackbar?: boolean) => void;
  deletedEntry: (id: string) => void;
}

const EntriesContext = createContext({} as EntriesContextProps);

export default EntriesContext;
