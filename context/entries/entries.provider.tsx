import { ReactNode, useEffect, useReducer } from 'react';
import { Entry } from '../../interfaces';
import { entriesApi } from '../../service';
import EntriesContext from './entries.context';
import { entriesReducer, entriesTypes } from './entries.reducer';
import { useSnackbar } from 'notistack';
export interface EntriesState {
  entries: Entry[];
}

const ENTRIES_INITIAL_STATE: EntriesState = {
  entries: [],
};

interface EntriesProviderProps {
  children: ReactNode | ReactNode[];
}

export default function EntriesProvider({ children }: EntriesProviderProps) {
  const [state, dispatch] = useReducer(entriesReducer, ENTRIES_INITIAL_STATE);
  const { enqueueSnackbar } = useSnackbar();

  const refreshEntries = async () => {
    const { data } = await entriesApi.get<Entry[]>('/entries');
    dispatch(entriesTypes.refreshDataAction(data));
  };

  useEffect(() => {
    refreshEntries();
  }, []);

  const addEntry = async (description: string) => {
    const { data } = await entriesApi.post<Entry>('/entries', { description });
    dispatch(entriesTypes.addEntryAction(data));
  };

  const deletedEntry = async (id: string) => {
    try {
      const { data } = await entriesApi.delete<Entry>(`/entries/${id}`);
      dispatch(entriesTypes.updateEntryAction(data));

      await refreshEntries();

      enqueueSnackbar('Entrada eliminada', {
        variant: 'success',
        autoHideDuration: 1500,
        anchorOrigin: {
          vertical: 'top',
          horizontal: 'right',
        },
      });
    } catch (error) {
      console.error(error);
    }
  };

  const updateEntry = async ({ _id, description, status }: Entry, showSnackbar = false) => {
    try {
      const { data } = await entriesApi.put<Entry>(`/entries/${_id}`, { description, status });
      dispatch(entriesTypes.updateEntryAction(data));
      if (showSnackbar) {
        enqueueSnackbar('Entrada actualizada', {
          variant: 'success',
          autoHideDuration: 1500,
          anchorOrigin: {
            vertical: 'top',
            horizontal: 'right',
          },
        });
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <EntriesContext.Provider value={{ ...state, addEntry, updateEntry, deletedEntry }}>
      {children}
    </EntriesContext.Provider>
  );
}
