import { createContext } from 'react';

interface UIContextProps {
  sideMenuOpen: boolean;
  isAddingEntry: boolean;
  isDragging: boolean;
  openSidebar: () => void;
  closeSidebar: () => void;
  setIsAddingEntry: (isAdding: boolean) => void;
  onToggleDragging: () => void;
}

const UIContext = createContext({} as UIContextProps);

export default UIContext;
