import { UIState } from './ui.provider';

type UIAction =
  | { type: 'OpenSidebar' }
  | { type: 'CloseSidebar' }
  | { type: 'toggleAddingEntry'; payload: boolean }
  | { type: 'toggleDragging' };

export function uiReducer(state: UIState, action: UIAction): UIState {
  switch (action.type) {
    case 'OpenSidebar':
      return { ...state, sideMenuOpen: true };
    case 'CloseSidebar':
      return { ...state, sideMenuOpen: false };
    case 'toggleAddingEntry':
      return { ...state, isAddingEntry: action.payload };
    case 'toggleDragging':
      return { ...state, isDragging: !state.isDragging };
    default:
      return state;
  }
}

function onCloseSidebar(): UIAction {
  return {
    type: 'CloseSidebar',
  };
}

function onOpenSidebar(): UIAction {
  return {
    type: 'OpenSidebar',
  };
}

function onToggleAddingEntry(isAddingEntry: boolean): UIAction {
  return {
    type: 'toggleAddingEntry',
    payload: isAddingEntry,
  };
}

function onToggleDragging(): UIAction {
  return {
    type: 'toggleDragging',
  };
}

export const uiTypes = {
  onCloseSidebar,
  onOpenSidebar,
  onToggleAddingEntry,
  onToggleDragging,
};
