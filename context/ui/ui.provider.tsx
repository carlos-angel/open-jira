import { ReactNode, useReducer } from 'react';
import UIContext from './ui.context';
import { uiReducer, uiTypes } from './ui.reducer';

export interface UIState {
  sideMenuOpen: boolean;
  isAddingEntry: boolean;
  isDragging: boolean;
}

const UI_INITIAL_STATE: UIState = {
  sideMenuOpen: false,
  isAddingEntry: false,
  isDragging: false,
};

interface UIProviderProps {
  children: ReactNode | ReactNode[];
}

export default function UIProvider({ children }: UIProviderProps) {
  const [state, dispatch] = useReducer(uiReducer, UI_INITIAL_STATE);

  const closeSidebar = () => dispatch(uiTypes.onCloseSidebar());
  const openSidebar = () => dispatch(uiTypes.onOpenSidebar());

  const setIsAddingEntry = (isAddingEntry: boolean) =>
    dispatch(uiTypes.onToggleAddingEntry(isAddingEntry));

  const onToggleDragging = () => dispatch(uiTypes.onToggleDragging());

  return (
    <UIContext.Provider
      value={{ ...state, openSidebar, closeSidebar, setIsAddingEntry, onToggleDragging }}
    >
      {children}
    </UIContext.Provider>
  );
}
