# OpenJira

## Requerimientos:

- docker y docker-compose
- makefile

## Instrucciones:

- `make build` construye los contenedores
- `make start` inicia los contenedores
- `make stop` detiene los contenedores
- `make restart` reinicia los contenedores

## Cargar datos de prueba en la base de datos:

llamar el endpoint:

```
http://localhost:3000/api/seed
```
