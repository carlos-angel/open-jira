import type { NextApiRequest, NextApiResponse } from 'next';
import { connectDatabase, disconnectDatabase, seedData } from '../../databases';
import { Entry } from '../../models';

type SeedData = {
  message: string;
};

export default async function handler(req: NextApiRequest, res: NextApiResponse<SeedData>) {
  if (process.env.NODE_ENV === 'production') {
    return res.status(401).json({ message: 'No access allowed' });
  }

  await connectDatabase();

  await Entry.deleteMany();
  await Entry.insertMany(seedData.entries);

  await disconnectDatabase();

  res.status(200).json({ message: 'proceso realizado correctamente' });
}
