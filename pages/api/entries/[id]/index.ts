import type { NextApiRequest, NextApiResponse } from 'next';
import { connectDatabase, disconnectDatabase } from '../../../../databases';
import { Entry } from '../../../../models';
import { IEntry } from '../../../../models/Entry.model';

type Data =
  | {
      message: string;
    }
  | IEntry;

export default function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  switch (req.method) {
    case 'PUT':
      return putEntry(req, res);
    case 'GET':
      return getEntry(req, res);
    case 'DELETE':
      return deletedEntry(req, res);
    default:
      return res.status(200).json({ message: 'endpoint Not found' });
  }
}

async function putEntry(req: NextApiRequest, res: NextApiResponse<Data>) {
  const { id } = req.query;

  try {
    await connectDatabase();
    const entryToUpdate = await Entry.findById(id);
    if (!entryToUpdate) {
      await disconnectDatabase();
      return res.status(404).json({ message: `La entrada con id ${id} no se encontró` });
    }

    const { description, status } = req.body;

    const updatedEntry = await Entry.findByIdAndUpdate(
      id,
      {
        description: description || entryToUpdate.description,
        status: status || entryToUpdate.status,
      },
      {
        runValidators: true,
        new: true,
      },
    );
    await disconnectDatabase();

    return res.status(200).json(updatedEntry!);
  } catch (error) {
    await disconnectDatabase();
    return res.status(500).json({ message: `Ops! Algo salo mal` });
  }
}

async function getEntry(req: NextApiRequest, res: NextApiResponse<Data>) {
  const { id } = req.query;
  try {
    const entry = await Entry.findById(id);
    if (!entry) {
      await disconnectDatabase();
      return res.status(404).json({ message: `La entrada con id ${id} no se encontró` });
    }

    return res.status(200).json(entry);
  } catch (error) {
    await disconnectDatabase();
    return res.status(500).json({ message: `Ops! Algo salo mal` });
  }
}

async function deletedEntry(req: NextApiRequest, res: NextApiResponse<Data>) {
  const { id } = req.query;
  try {
    const entry = await Entry.findById(id);
    if (!entry) {
      await disconnectDatabase();
      return res.status(404).json({ message: `La entrada con id ${id} no se encontró` });
    }

    await Entry.findByIdAndRemove(id);

    return res.status(200).json({ message: 'Entrada eliminada' });
  } catch (error) {
    await disconnectDatabase();
    return res.status(500).json({ message: `Ops! Algo salo mal` });
  }
}
