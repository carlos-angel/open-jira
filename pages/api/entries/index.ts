import type { NextApiRequest, NextApiResponse } from 'next';
import { connectDatabase, disconnectDatabase } from '../../../databases';
import { Entry, IEntry } from '../../../models';

type EntriesData =
  | {
      message: string;
    }
  | IEntry[]
  | IEntry;

export default function handler(req: NextApiRequest, res: NextApiResponse<EntriesData>) {
  switch (req.method) {
    case 'GET':
      return getEntries(res);
    case 'POST':
      return postEntry(req, res);
    default:
      return res.status(200).json({ message: 'endpoint Not found' });
  }
}

async function getEntries(res: NextApiResponse<EntriesData>) {
  await connectDatabase();

  const entries = await Entry.find().sort({ createdAt: 'ascending' });

  await disconnectDatabase();

  res.status(200).json(entries);
}

async function postEntry(req: NextApiRequest, res: NextApiResponse<EntriesData>) {
  const { description = '' } = req.body;
  const newEntry = new Entry({
    description,
    createdAt: Date.now(),
  });

  try {
    await connectDatabase();
    await newEntry.save();
    await disconnectDatabase();
    res.status(201).json(newEntry);
  } catch (error) {
    await disconnectDatabase();
    res.status(500).json({ message: 'Ops! Algo salio mal.' });
  }
}
